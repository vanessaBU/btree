#ifndef BTREE_NODE_HPP
#define BTREE_NODE_HPP

#include <iostream>
#include <memory>
#include <vector>

#include "BTreeKey.hpp"

template <typename KEYTYPE, typename DATATYPE>
class BTreeNode : public std::enable_shared_from_this<BTreeNode<KEYTYPE, DATATYPE> >
{
	typedef BTreeKey<KEYTYPE, DATATYPE> BKey;
	public:
		BTreeNode() : t(2) // default to max 3 keys and max 4 child nodes
		{
			keys.reserve(4); // reserve memory for max number of keys + 1 (for insert/split)
			children.reserve(5); // reserve memory for max child nodes + 1 (for absorb/split)
		}
		BTreeNode(int min_degree) : t(min_degree)
		{
			int maxDegree = 2 * t; // max number of child nodes
			keys.reserve(maxDegree); // reserve memory for max number of keys + 1 (for insert/split)
			children.reserve(maxDegree); // reserve memory for max child nodes
		}
		void insert(KEYTYPE key_val, DATATYPE dat) // this insert should always start at root
		{
			std::unique_ptr<BKey> bkey = std::make_unique<BKey>(key_val, dat); // create key pointer
			this->insert(std::move(bkey)); // insert key pointer
		}
		bool insert(std::unique_ptr<BKey>&& bkey)
		{
			// return true if split occured and parent needs to absorb child node
			//
			// 1. find insert position
			int i; // insert position
			int n = std::max(keys.size(), children.size());
			for (i = 0; i < n; ++i)
			{
				if (i >= keys.size() || (keys[i]->value > bkey->value))
				{
					break; // break if passed final key or found first key > new key
				}
			}
			// 2. insert key if leaf node or send key to subtree
			if (isLeaf()) // insert key in current leaf node
			{
				if (i >= keys.size()) // push to back if iterated past final key
				{
					keys.push_back(std::move(bkey));
				}
				else // insert before iterated position
				{
					keys.insert(keys.begin() + i, std::move(bkey));
				}
			}
			else // send key to subtree
			{
				if (children[i]->insert(std::move(bkey)))
				{
					this->absorbChild(i); // if insert subtree returns true then absorb child node
				}
			}
			// 3. check total number of keys
			if (keys.size() == keys.capacity()) // if exceed max keys then split
			{
				/*std::cout << "print this before split:\t";
				this->printKeys();
				std::cout << std::endl;*/
				this->split();
				return true; // if node splits, tell parent to absorb this node
			}
			else // split not needed
			{
				return false; // parent does not need to absorb
			}
		}
		void absorbChild(int i)
		{
			std::cout << "absorbChild()\n";
			// 1. make copies of children of chil node
			std::shared_ptr<BTreeNode> L = children[i]->children[0];
			std::shared_ptr<BTreeNode> R = children[i]->children[1];
			// 2. move child key to current keys in same position as child index
			keys.insert(keys.begin() + i, std::move(children[i]->keys.front()));
			// 3. add left and right child nodes in same position as child index
			children.insert(children.begin() + i, R); // first add right child
			children.insert(children.begin() + i, L); // next add left child
			// 4. delete old child pointer
			children.erase(children.begin() + i + 2);
		}
		void split()
		{
			std::cout << "split()\n";
			// note mid key index to remain in current node
			int mkey = midKey();
			int n = std::max(keys.size(), children.size()); // remember original max keys and child sizes
			// 1. create new nodes for left and right children
			std::shared_ptr<BTreeNode> L = std::make_shared<BTreeNode>();
			std::shared_ptr<BTreeNode> R = std::make_shared<BTreeNode>();
			// 2. add keys and children to left node
			for (int i = 0; i <= mkey; ++i)
			{
				if (i < mkey) // add keys up to and not including mid key
				{
					L->keys.push_back(std::move(keys.front())); // add key to new node
					keys.erase(keys.begin()); // erase key from current
				}
				if (!isLeaf()) // add children up to and including mid key index number
				{
					L->children.push_back(std::move(children.front())); // add child to new node
					children.erase(children.begin()); // erase child from current
				}
			}
			// 3. add keys and children to right node
			for (int i = (mkey + 1); i < n; ++i)
			{
				if (keys.size() > 1) // add keys up to and including last key
				{
					R->keys.push_back(std::move(keys[1])); // add key to new node
					keys.erase(keys.begin() + 1); // erase key from current
				}
				if (!isLeaf()) // add children up to and including mid key index number
				{
					R->children.push_back(std::move(children[0])); // add child to new node
					children.erase(children.begin() + 1); // erase child from current
				}
			}
			// 4. set left and right nodes as children of current node
			children.push_back(std::move(L));
			children.push_back(std::move(R));
			// test print
			/*std::cout << "printing left:\t";
			children[0]->printKeys();
			std::cout << "\nprinting this:\t";
			this->printKeys();
			std::cout << "\nprinting right:\t";
			children[1]->printKeys();
			std::cout << std::endl;*/
		}
		void print()
		{
			for (int i = 0; i < keys.size(); ++i)
			{
				if (!isLeaf()) // print i-th subtree first if not leaf
				{
					children[i]->print();
				}
				std::cout << "[" << keys[i]->value << "]"; // print i-th key second
				std::cout << keys[i]->data << " ";
			}
			if (!isLeaf()) // print final subtree if not leaf
			{
				(children.back())->print();
			}
		}
		void printKeys() // only print the keys in current node
		{
			for (int i = 0; i < keys.size(); ++i)
			{
				std::cout << "[" << keys[i]->value << "]";
				std::cout << keys[i]->data << " ";
			}
		}
		//std::shared_ptr<BTreeNode<KEYTYPE, DATATYPE> > search(KEYTYPE key) // return null if key not found
		std::shared_ptr<BTreeNode> search(KEYTYPE key) // return null if key not found
		{
			int i; // key position
			int n = std::max(keys.size(), children.size());
			for (i = 0; i < n; ++i)
			{
				if (i >= keys.size() || (keys[i]->value > key))
				{
					break; // break if passed final key or found first key > search key
				}
				else if (keys[i]->value == key)
				{
					return this->shared_from_this(); // return this node if key found
				}
			}
			if (isLeaf())
			{
				return nullptr; // return null if is leaf and key not found
			}
			else
			{
				return children[i]->search(key); // search subtree
			}
		}
		friend class BTree; // give btree access to node properties
	private:
		int t; // minimum degree
		std::vector<std::unique_ptr<BKey> > keys; // container of key pointers
		std::vector<std::shared_ptr<BTreeNode> > children; // container of child node pointers
		bool isLeaf() { return children.empty(); }
		int midKey() { return ((keys.size() - 1) / 2); }
};

#endif
