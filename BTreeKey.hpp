#ifndef BTREE_KEY_HPP
#define BTREE_KEY_HPP

template <typename KEYTYPE, typename DATATYPE>
class BTreeKey
{
	public:
		BTreeKey() {}
		BTreeKey(KEYTYPE val, DATATYPE dat) : value(val), data(dat) {}
		KEYTYPE value;
		DATATYPE data;
};

#endif
