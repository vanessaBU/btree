#include <iostream>
#include <memory>

#include "BTreeNode.hpp"

int main()
{
	typedef BTreeNode<int, char> BNode;
	
	/*BNode node;
	
	node.insert(1, 'a');
	node.print();
	std::cout << "\n";
	
	node.insert(2, 'b');
	node.print();
	std::cout << "\n";
	
	node.insert(3, 'c');
	node.print();
	std::cout << "\n";*/
	
	std::shared_ptr<BNode> b = std::make_shared<BNode>();
	/*b->insert(1, 'a');
	b->insert(2, 'b');
	b->insert(3, 'c');
	b->insert(4, 'd');
	std::shared_ptr<BNode> p = b->search(2);*/
	b->insert(10, 'a');
	b->insert(30, 'b');
	b->insert(40, 'c');
	b->insert(20, 'd');
	b->insert(5, 'e');
	b->insert(3, 'f');
	b->insert(8, 'g');
	std::shared_ptr<BNode> p = b->search(8);
	if (p)
	{
		p->printKeys();
	}
	else
	{
		std::cout << "not found!";
	}
	std::cout << "\nprint all:\n";
	b->print();
	
	return 0;
}
